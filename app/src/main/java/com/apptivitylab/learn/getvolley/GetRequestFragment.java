package com.apptivitylab.learn.getvolley;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


/**
 * A simple {@link Fragment} subclass.
 */
public class GetRequestFragment extends Fragment {
    private EditText mUrlEditText;
    private Button mLoadButton;
    private ProgressBar mProgressBar;
    private TextView mResultTextView;

    public GetRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_get_request, container, false);

        mUrlEditText = (EditText) rootView.findViewById(R.id.fragment_get_request_et_url);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.fragment_get_request_progress);
        mLoadButton = (Button) rootView.findViewById(R.id.fragment_get_request_btn_load);
        mResultTextView = (TextView) rootView.findViewById(R.id.fragment_get_request_tv_results);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        mLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadHtmlString();
            }
        });
    }

    private void loadHtmlString() {
        // Prepare URL
        String url = null;
        if (mUrlEditText.getText().toString().length() > 0) {
            url = mUrlEditText.getText().toString();
        } else  {
            url = "https://developer.android.com/index.html";
            mUrlEditText.setText(url);
        }

        // Prepare a requestQueue
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        // Create a StringRequest to load URL contents
        mProgressBar.setIndeterminate(true);

        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mResultTextView.setText(response);
                mProgressBar.setIndeterminate(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mResultTextView.setText(error.toString());
                mProgressBar.setIndeterminate(false);
            }
        });

        requestQueue.add(request);
    }
}
